package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.recruitments.bit4mation.persistence.database.data.CreateRoot;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class AddRootEventRecreate extends EventRecreate {

    public AddRootEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        CreateRoot createRoot = objectMapper.readValue(payload, CreateRoot.class);
        LeafEntity root = leafEntityManagement.createRoot();
        pastIdToPresentIdHolder.registerLeaf(createRoot.getLeafId(), root.getLeafId());
    }

}
