package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.recruitments.bit4mation.persistence.database.data.MoveLeaf;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class MoveLeafEventRecreate extends EventRecreate {

    public MoveLeafEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        MoveLeaf moveLeaf = objectMapper.readValue(payload, MoveLeaf.class);
        leafEntityManagement.moveLeaf(moveLeaf.getLeafId(), moveLeaf.getNewParentLeafId(), false);
    }

}
