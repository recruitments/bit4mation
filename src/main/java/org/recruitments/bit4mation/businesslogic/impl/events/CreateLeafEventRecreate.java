package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.recruitments.bit4mation.persistence.database.data.AddLeaf;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class CreateLeafEventRecreate extends EventRecreate {

    public CreateLeafEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        AddLeaf addLeaf = objectMapper.readValue(payload, AddLeaf.class);
        LeafEntity leaf = leafEntityManagement.createLeaf(pastIdToPresentIdHolder.getLeaf(addLeaf.getParentLeafId()), addLeaf.getWeight(), false);
        pastIdToPresentIdHolder.registerLeaf(addLeaf.getLeafId(), leaf.getLeafId());
//        pastIdToPresentIdHolder.registerNode(leafEntityManagement.);
    }

}
