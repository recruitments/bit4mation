package org.recruitments.bit4mation.businesslogic.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.LeafCreateManagement;
import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.mappers.LeafEntityToTreeMapper;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class LeafCreateManagementImpl implements LeafCreateManagement {

    private final LeafEntityToTreeMapper leafEntityToTreeMapper = new LeafEntityToTreeMapper();

    private final LeafEntityManagement leafEntityManagement;

    public Leaf createLeaf(Long parentLeafId) {
        return createLeaf(parentLeafId, 0);
    }

    public Leaf createLeaf(Long parentLeafId, Integer weight) {
        LeafEntity leaf = leafEntityManagement.createLeaf(parentLeafId, weight);
        return leafEntityToTreeMapper.map(leaf);
    }

}
