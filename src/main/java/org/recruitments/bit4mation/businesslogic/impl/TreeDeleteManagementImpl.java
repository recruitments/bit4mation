package org.recruitments.bit4mation.businesslogic.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.TreeDeleteManagement;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TreeDeleteManagementImpl implements TreeDeleteManagement {

    private final LeafEntityManagement leafEntityManagement;

    public void deleteLeaf(Long leafId) {
        leafEntityManagement.deleteLeaf(leafId);
    }

    public void deleteNode(Long nodeId) {
        leafEntityManagement.deleteNode(nodeId);
    }

}
