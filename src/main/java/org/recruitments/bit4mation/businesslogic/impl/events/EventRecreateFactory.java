package org.recruitments.bit4mation.businesslogic.impl.events;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.persistence.database.data.EventEnum;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

@RequiredArgsConstructor
public class EventRecreateFactory {

    private final LeafEntityManagement leafEntityManagement;

    public EventRecreate createEventRecreateFromEvent(PastIdToPresentIdHolder pastIdToPresentIdHolder, EventEnum eventEnum) {
        switch (eventEnum) {
            case ADD_ROOT:
                return new AddRootEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
            case CREATE_LEAF:
                return new CreateLeafEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
            case DELETE_LEAF:
                return new DeleteLeafEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
            case DELETE_NODE:
                return new DeleteNodeEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
            case MOVE_NODE:
                return new MoveNodeEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
            case MOVE_LEAF:
                return new MoveLeafEventRecreate(pastIdToPresentIdHolder, leafEntityManagement);
        }
        return null;
    }

}
