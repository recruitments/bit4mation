package org.recruitments.bit4mation.businesslogic.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.RestoreFromSnapshot;
import org.recruitments.bit4mation.businesslogic.impl.events.EventRecreate;
import org.recruitments.bit4mation.businesslogic.impl.events.EventRecreateFactory;
import org.recruitments.bit4mation.businesslogic.impl.events.PastIdToPresentIdHolder;
import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.recruitments.bit4mation.persistence.database.service.EventEntityManagement;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@RequiredArgsConstructor
@Service
public class RestoreFromSnapshotImpl implements RestoreFromSnapshot {

    private final LeafEntityManagement leafEntityManagement;

    private final EventEntityManagement eventEntityManagement;

    private EventRecreateFactory eventRecreateFactory;

    @PostConstruct
    public void postConstruct() {
        eventRecreateFactory = new EventRecreateFactory(leafEntityManagement);
    }

    @Override
    public Leaf restoreFromSnapshot(Long snapshotId) {
        leafEntityManagement.deleteAll();
        PastIdToPresentIdHolder pastIdToPresentIdHolder = new PastIdToPresentIdHolder();
        List<EventEntity> events = eventEntityManagement.getAllEventsToRecreateDBFromSnapshot(snapshotId);
        events.forEach((event) -> performEvent(event, pastIdToPresentIdHolder));
        return null;
    }

    private void performEvent(EventEntity eventEntity, PastIdToPresentIdHolder pastIdToPresentIdHolder) {
        EventRecreate eventRecreate = eventRecreateFactory.createEventRecreateFromEvent(pastIdToPresentIdHolder, eventEntity.getName());
        try {
            eventRecreate.execute(eventEntity.getPayload());
        } catch (JsonProcessingException e) {
            e.printStackTrace();//TODO: handle
        }
    }

}
