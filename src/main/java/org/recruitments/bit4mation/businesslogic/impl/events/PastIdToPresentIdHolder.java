package org.recruitments.bit4mation.businesslogic.impl.events;

import java.util.HashMap;
import java.util.Map;

public class PastIdToPresentIdHolder {

    private Map<Long, Long> oldLeafIdsAndNewOnes = new HashMap<>();

    private Map<Long, Long> oldNodeIdsAndNewOnes = new HashMap<>();

    public void registerLeaf(Long pastId, Long newId) {
        this.oldLeafIdsAndNewOnes.put(pastId, newId);
    }

    public Long getLeaf(Long pastId) {
        return this.oldLeafIdsAndNewOnes.get(pastId);
    }

    public void registerNode(Long pastId, Long newId) {
        this.oldNodeIdsAndNewOnes.put(pastId, newId);
    }

    public Long getNode(Long pastId) {
        return this.oldNodeIdsAndNewOnes.get(pastId);
    }

}
