package org.recruitments.bit4mation.businesslogic.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.CreateSnapshot;
import org.recruitments.bit4mation.persistence.database.service.SnapshotEntityManagement;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CreateSnapshotImpl implements CreateSnapshot {

    private final SnapshotEntityManagement snapshotEntityManagement;

    public Long createSnapshot() {
        return snapshotEntityManagement.createSnapshot().getSnapshotId();
    }

}
