package org.recruitments.bit4mation.businesslogic.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.TreeMoveManagement;
import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TreeMoveManagementImpl implements TreeMoveManagement {

    private final LeafEntityManagement leafEntityManagement;

    public void moveLeaf(Long leafId, Long newParentLeafId) {
        leafEntityManagement.moveLeaf(leafId, newParentLeafId);
    }

    public void moveNode(Long nodeId, Long newParentLeafId) {
        leafEntityManagement.moveNode(nodeId, newParentLeafId);
    }

}
