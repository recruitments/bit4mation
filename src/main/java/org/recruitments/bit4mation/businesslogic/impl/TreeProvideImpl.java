package org.recruitments.bit4mation.businesslogic.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.TreeProvide;
import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.data.Node;
import org.recruitments.bit4mation.mappers.LeafEntityToTreeMapper;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TreeProvideImpl implements TreeProvide {

    private final LeafEntityToTreeMapper leafEntityToTreeMapper = new LeafEntityToTreeMapper();

    private final LeafEntityManagement leafEntityManagement;

    @Override
    public Leaf provideTree() {
        LeafEntity root = provideRoot();
        return leafEntityToTreeMapper.map(root);
    }

    @Override
    public Leaf provideTreeFromLeaf(Long leafId) {
        return null;
    }

    private LeafEntity provideRoot() {
        Optional<LeafEntity> root = leafEntityManagement.provideRoot();
        return root.or(() -> Optional.of(leafEntityManagement.createRoot())).get();
    }

}
