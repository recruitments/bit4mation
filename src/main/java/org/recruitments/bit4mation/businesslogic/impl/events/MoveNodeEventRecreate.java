package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.recruitments.bit4mation.persistence.database.data.MoveNode;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class MoveNodeEventRecreate extends EventRecreate {

    public MoveNodeEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        MoveNode moveNode = objectMapper.readValue(payload, MoveNode.class);
        leafEntityManagement.moveNode(moveNode.getNodeId(), moveNode.getNewParentLeafId(), false);
    }
}
