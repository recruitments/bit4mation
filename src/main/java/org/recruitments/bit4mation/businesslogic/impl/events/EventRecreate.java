package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public abstract class EventRecreate {

    protected final PastIdToPresentIdHolder pastIdToPresentIdHolder;

    protected final LeafEntityManagement leafEntityManagement;

    public EventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        this.pastIdToPresentIdHolder = pastIdToPresentIdHolder;
        this.leafEntityManagement = leafEntityManagement;
    }

    abstract public void execute(String payload) throws JsonProcessingException;

}
