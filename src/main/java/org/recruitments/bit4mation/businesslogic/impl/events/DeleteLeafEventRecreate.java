package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class DeleteLeafEventRecreate extends EventRecreate {

    public DeleteLeafEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        leafEntityManagement.deleteLeaf(pastIdToPresentIdHolder.getLeaf(Long.valueOf(payload)), false);
    }

}
