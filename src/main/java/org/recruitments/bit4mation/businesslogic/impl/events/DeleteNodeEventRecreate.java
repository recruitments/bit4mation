package org.recruitments.bit4mation.businesslogic.impl.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;

public class DeleteNodeEventRecreate extends EventRecreate {

    public DeleteNodeEventRecreate(PastIdToPresentIdHolder pastIdToPresentIdHolder, LeafEntityManagement leafEntityManagement) {
        super(pastIdToPresentIdHolder, leafEntityManagement);
    }

    @Override
    public void execute(String payload) throws JsonProcessingException {
        leafEntityManagement.deleteNode(Long.valueOf(payload), false);
    }

}
