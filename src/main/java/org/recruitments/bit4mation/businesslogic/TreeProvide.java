package org.recruitments.bit4mation.businesslogic;

import org.recruitments.bit4mation.data.Leaf;

public interface TreeProvide {

    Leaf provideTree();

    Leaf provideTreeFromLeaf(Long leafId);

}
