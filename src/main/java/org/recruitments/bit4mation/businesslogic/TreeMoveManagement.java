package org.recruitments.bit4mation.businesslogic;

import org.recruitments.bit4mation.data.Leaf;

public interface TreeMoveManagement {

    void moveLeaf(Long leafId, Long newParentLeafId);

    void moveNode(Long nodeId, Long newParentLeafId);

}
