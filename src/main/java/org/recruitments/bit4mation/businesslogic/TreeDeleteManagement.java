package org.recruitments.bit4mation.businesslogic;

public interface TreeDeleteManagement {

    void deleteLeaf(Long leafId);

    void deleteNode(Long nodeId);

}
