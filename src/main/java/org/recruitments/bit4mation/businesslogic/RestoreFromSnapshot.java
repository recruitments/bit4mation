package org.recruitments.bit4mation.businesslogic;

import org.recruitments.bit4mation.data.Leaf;
import org.springframework.transaction.annotation.Transactional;

public interface RestoreFromSnapshot {

    @Transactional
    Leaf restoreFromSnapshot(Long snapshotId);

}
