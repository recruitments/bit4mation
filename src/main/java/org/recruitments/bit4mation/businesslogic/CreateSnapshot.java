package org.recruitments.bit4mation.businesslogic;

public interface CreateSnapshot {

    Long createSnapshot();

}
