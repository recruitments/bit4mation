package org.recruitments.bit4mation.businesslogic;

import org.recruitments.bit4mation.data.Leaf;

public interface LeafCreateManagement {

    Leaf createLeaf(Long parentLeafId);

    Leaf createLeaf(Long parentLeafId, Integer weight);

}
