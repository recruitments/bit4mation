package org.recruitments.bit4mation.persistence.database.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MoveNode {

    private Long nodeId;

    private Long newParentLeafId;

}
