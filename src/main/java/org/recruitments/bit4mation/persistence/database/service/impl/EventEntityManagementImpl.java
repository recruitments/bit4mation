package org.recruitments.bit4mation.persistence.database.service.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.recruitments.bit4mation.persistence.database.entites.SnapshotEntity;
import org.recruitments.bit4mation.persistence.database.repositories.EventRepository;
import org.recruitments.bit4mation.persistence.database.repositories.SnapshotRepository;
import org.recruitments.bit4mation.persistence.database.service.EventEntityManagement;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class EventEntityManagementImpl implements EventEntityManagement {

    private final SnapshotRepository snapshotRepository;

    private final EventRepository eventRepository;

    @Override
    public List<EventEntity> getAllEventsToRecreateDBFromSnapshot(Long snapshotEntity) {
        return eventRepository.findAllByCreatedAtBeforeOrderByCreatedAtAsc(snapshotRepository.getOne(snapshotEntity).getSnapshotToEvent().getCreatedAt());
    }

}
