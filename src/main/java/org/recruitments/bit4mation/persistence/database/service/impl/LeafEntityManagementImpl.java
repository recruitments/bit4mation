package org.recruitments.bit4mation.persistence.database.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.persistence.database.data.*;
import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.entites.NodeEntity;
import org.recruitments.bit4mation.persistence.database.repositories.EventRepository;
import org.recruitments.bit4mation.persistence.database.repositories.LeafRepository;
import org.recruitments.bit4mation.persistence.database.repositories.NodeRepository;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LeafEntityManagementImpl implements LeafEntityManagement {

    private final LeafRepository leafRepository;

    private final NodeRepository nodeRepository;

    private final EventRepository eventRepository;

    @Override
    public Optional<LeafEntity> provideRoot(boolean registerEvent) {
        return leafRepository.findByIsRoot(true);
    }

    @Override
    public LeafEntity createRoot(boolean registerEvent) {
        try {
            LeafEntity leaf = leafRepository.save(LeafEntity.rootLeaf());
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.ADD_ROOT, new CreateRoot(leaf.getLeafId())));
            }
            return leaf;
        } catch (JsonProcessingException e) {
            //FIXME: exception handling -> and errors
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public LeafEntity createLeaf(Long parentLeafId, boolean registerEvent) {
        return createLeaf(parentLeafId, 0);
    }

    @Override
    public LeafEntity createLeaf(Long parentLeafId, Integer weight, boolean registerEvent) {
        try {
            Optional<LeafEntity> parentLeaf = leafRepository.findById(parentLeafId);
            LeafEntity leafEntity = new LeafEntity(weight + parentLeaf.get().getValue());
            LeafEntity childrenLeaf = leafRepository.save(leafEntity);
            NodeEntity nodeEntity = new NodeEntity(parentLeaf.get(), childrenLeaf, weight);
            NodeEntity node = nodeRepository.save(nodeEntity);
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.CREATE_LEAF, new AddLeaf(leafEntity.getLeafId(), node.getNodeId(), parentLeafId, weight)));
            }
            return leafEntity;
        } catch (JsonProcessingException e) {
            //FIXME: exception handling -> and errors
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteLeaf(Long leafId, boolean registerEvent) {
        try {
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.DELETE_LEAF, Long.valueOf(leafId)));
            }
            deleteNode(nodeRepository.findByChildren_LeafId(leafId));
        } catch (JsonProcessingException e) {
            //FIXME: handling
            e.printStackTrace();
        }
    }

    @Override
    public void deleteNode(Long nodeId, boolean registerEvent) {
        try {
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.DELETE_NODE, Long.valueOf(nodeId)));
            }
            NodeEntity nodeEntity = nodeRepository.findById(nodeId).get();
            deleteNode(nodeEntity);
        } catch (JsonProcessingException e) {
            //FIXME: handling
            e.printStackTrace();
        }

    }

    @Override
    public void moveLeaf(Long leafId, Long newParentLeafId, boolean registerEvent) {
        try {
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.MOVE_LEAF, new MoveLeaf(leafId, newParentLeafId)));
            }
            NodeEntity nodeForLeaf = nodeRepository.findByChildren_LeafId(leafId);
            LeafEntity newParentLeaf = leafRepository.findById(newParentLeafId).get();
            LeafEntity currentLeaf = leafRepository.findById(leafId).get();
            nodeForLeaf.setWeight(0);
            nodeForLeaf.setParent(newParentLeaf);
            int difference = currentLeaf.getValue() - newParentLeaf.getValue();
            adjustValueForLeafAfterMovingFromParent(currentLeaf, difference);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            //FIXME: handling
        }
    }

    @Override
    public void moveNode(Long nodeId, Long newParentLeafId, boolean registerEvent) {
        try {
            if (registerEvent) {
                eventRepository.save(EventEntity.of(EventEnum.MOVE_NODE, new MoveNode(nodeId, newParentLeafId)));
            }
            NodeEntity node = nodeRepository.findById(nodeId).get();
            LeafEntity newParentLeaf = leafRepository.findById(newParentLeafId).get();
            node.setParent(newParentLeaf);
            int difference = node.getChildren().getValue() - newParentLeaf.getValue();
            adjustValueForLeafAfterMovingFromParent(node.getChildren(), difference);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            //FIXME: handling
        }
    }

    @Override
    public Optional<LeafEntity> provideRoot() {
        return provideRoot(true);
    }

    @Override
    public LeafEntity createRoot() {
        return createRoot(true);
    }

    @Override
    public LeafEntity createLeaf(Long parentLeafId) {
        return createLeaf(parentLeafId, true);
    }

    @Override
    public LeafEntity createLeaf(Long parentLeafId, Integer weight) {
        return createLeaf(parentLeafId, weight, true);
    }

    @Override
    public void deleteLeaf(Long leafId) {
        deleteLeaf(leafId, true);
    }

    @Override
    public void deleteNode(Long nodeId) {
        deleteNode(nodeId, true);
    }

    @Override
    public void moveLeaf(Long leafId, Long newParentLeafId) {
        moveLeaf(leafId, newParentLeafId, true);
    }

    @Override
    public void moveNode(Long nodeId, Long newParentLeafId) {
        moveNode(nodeId, newParentLeafId, true);
    }

    @Override
    public void deleteAll() {
        LeafEntity leafEntity = provideRoot()
                .get();
        leafEntity
                .getNodes()
                .forEach((node) -> deleteNode(node));
        leafRepository.delete(leafEntity);
    }

    private void deleteNode(NodeEntity nodeEntity) {
        LeafEntity children = nodeEntity.getChildren();
        if (children.getNodes().size() > 0) {
            children.getNodes().forEach((node) -> deleteNode(node));
        }
        leafRepository.delete(children);
        nodeRepository.delete(nodeEntity);
    }

    private void adjustValueForLeafAfterMovingFromParent(LeafEntity leafEntity, int difference) {
        leafEntity.setValue(leafEntity.getValue() - difference);
        leafRepository.save(leafEntity);
        leafEntity.getNodes().forEach((node) -> {
            if (node.getChildren() != null) {
                adjustValueForLeafAfterMovingFromParent(node.getChildren(), difference);
            }
        });
    }

}
