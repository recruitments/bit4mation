package org.recruitments.bit4mation.persistence.database.entites;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "snapshots")
public class SnapshotEntity {

    @Id
    @GeneratedValue
    private Long snapshotId;

    @OneToOne
    @JoinColumn(name = "event_id")
    @NonNull
    private EventEntity snapshotToEvent;

    @Column
    @CreationTimestamp
    private Timestamp createdAt;

}
