package org.recruitments.bit4mation.persistence.database.data;

public enum EventEnum {

    ADD_ROOT,
    CREATE_LEAF,
    DELETE_LEAF,
    DELETE_NODE,
    MOVE_NODE,
    MOVE_LEAF

}
