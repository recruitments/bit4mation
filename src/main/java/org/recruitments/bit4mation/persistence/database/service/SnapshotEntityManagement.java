package org.recruitments.bit4mation.persistence.database.service;

import org.recruitments.bit4mation.persistence.database.entites.SnapshotEntity;

public interface SnapshotEntityManagement {

    SnapshotEntity createSnapshot();

    SnapshotEntity getById(Long snapshotId);

}
