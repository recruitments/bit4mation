package org.recruitments.bit4mation.persistence.database.data;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddLeaf {

    private Long leafId;

    private Long nodeId;

    private Long parentLeafId;

    private Integer weight;

}
