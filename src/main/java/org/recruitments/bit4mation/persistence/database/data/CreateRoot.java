package org.recruitments.bit4mation.persistence.database.data;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreateRoot {

    private Long leafId;

}
