package org.recruitments.bit4mation.persistence.database.entites;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "leafs")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LeafEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long leafId;

    @Column
    private Integer value;

    @Column
    private boolean isRoot = false;

    @OneToMany(mappedBy = "parent")
    private List<NodeEntity> nodes = new ArrayList<>();

    public LeafEntity(Integer value) {
        this.value = value;
    }

    public LeafEntity(Integer value, boolean isRoot) {
        this.value = value;
        this.isRoot = isRoot;
    }

    public static final LeafEntity rootLeaf() {
        return new LeafEntity(0, true);
    }

}
