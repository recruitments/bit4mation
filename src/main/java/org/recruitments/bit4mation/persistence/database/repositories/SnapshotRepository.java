package org.recruitments.bit4mation.persistence.database.repositories;

import org.recruitments.bit4mation.persistence.database.entites.SnapshotEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SnapshotRepository extends JpaRepository<SnapshotEntity, Long> {

    Optional<SnapshotEntity> findFirstByOrderByCreatedAtDesc();

}
