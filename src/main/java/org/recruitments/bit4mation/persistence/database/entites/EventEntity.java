package org.recruitments.bit4mation.persistence.database.entites;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.recruitments.bit4mation.persistence.database.data.EventEnum;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "events")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
public class EventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long eventId;

    @Column
    @Enumerated(EnumType.STRING)
    @NonNull
    private EventEnum name;

    @Column
    @NonNull
    private String payload;

    @Column
    @CreationTimestamp
    private Timestamp createdAt;

    public static <T> EventEntity of(EventEnum event, T data) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return new EventEntity(event, objectMapper.writeValueAsString(data));
    }

}
