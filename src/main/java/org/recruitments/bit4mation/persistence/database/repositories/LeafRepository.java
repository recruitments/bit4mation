package org.recruitments.bit4mation.persistence.database.repositories;

import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LeafRepository extends JpaRepository<LeafEntity, Long> {

    Optional<LeafEntity> findByIsRoot(boolean isRoot);

}
