package org.recruitments.bit4mation.persistence.database.service;

import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface LeafEntityManagement {

    Optional<LeafEntity> provideRoot(boolean registerEvent);

    @Transactional
    LeafEntity createRoot(boolean registerEvent);

    @Transactional
    LeafEntity createLeaf(Long parentLeafId, boolean registerEvent);

    @Transactional
    LeafEntity createLeaf(Long parentLeafId, Integer weight, boolean registerEvent);

    @Transactional
    void deleteLeaf(Long leafId, boolean registerEvent);

    @Transactional
    void deleteNode(Long nodeId, boolean registerEvent);

    @Transactional
    void moveLeaf(Long leafId, Long newParentLeafId, boolean registerEvent);

    @Transactional
    void moveNode(Long nodeId, Long newParentLeafId, boolean registerEvent);

    Optional<LeafEntity> provideRoot();

    @Transactional
    LeafEntity createRoot();

    @Transactional
    LeafEntity createLeaf(Long parentLeafId);

    @Transactional
    LeafEntity createLeaf(Long parentLeafId, Integer weight);

    @Transactional
    void deleteLeaf(Long leafId);

    @Transactional
    void deleteNode(Long nodeId);

    @Transactional
    void moveLeaf(Long leafId, Long newParentLeafId);

    @Transactional
    void moveNode(Long nodeId, Long newParentLeafId);

    @Transactional
    void deleteAll();
}
