package org.recruitments.bit4mation.persistence.database.repositories;

import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface EventRepository extends JpaRepository<EventEntity, Long> {

    Optional<EventEntity> findFirstByOrderByCreatedAtDesc();

    List<EventEntity> findAllByCreatedAtBeforeOrderByCreatedAtAsc(Timestamp createdAt);
}
