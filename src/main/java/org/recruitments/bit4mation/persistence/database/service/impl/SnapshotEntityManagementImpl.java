package org.recruitments.bit4mation.persistence.database.service.impl;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.recruitments.bit4mation.persistence.database.entites.SnapshotEntity;
import org.recruitments.bit4mation.persistence.database.repositories.EventRepository;
import org.recruitments.bit4mation.persistence.database.repositories.SnapshotRepository;
import org.recruitments.bit4mation.persistence.database.service.SnapshotEntityManagement;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class SnapshotEntityManagementImpl implements SnapshotEntityManagement {

    private final SnapshotRepository snapshotRepository;

    private final EventRepository eventRepository;

    @Override
    public SnapshotEntity createSnapshot() {
        Optional<SnapshotEntity> snapshot = snapshotRepository.findFirstByOrderByCreatedAtDesc();
        if (snapshot.isEmpty() || !snapshotForNewestEventExists(snapshot.get())) {
            Optional<EventEntity> newestEvent = eventRepository.findFirstByOrderByCreatedAtDesc();
            if (newestEvent.isPresent()) {
                SnapshotEntity newSnapshot = new SnapshotEntity(newestEvent.get());
                return snapshotRepository.save(newSnapshot);
            }
        }
        return null;//TODO: handle
    }

    @Override
    public SnapshotEntity getById(Long snapshotId) {
        return snapshotRepository.getOne(snapshotId);
    }

    private boolean snapshotForNewestEventExists(SnapshotEntity snapshotEntity) {
        return snapshotEntity
                .getSnapshotToEvent()
                .getEventId()
                .equals(
                        eventRepository
                                .findFirstByOrderByCreatedAtDesc()
                                .orElse(null)
                                .getEventId());
    }

}
