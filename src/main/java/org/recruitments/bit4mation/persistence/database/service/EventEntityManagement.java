package org.recruitments.bit4mation.persistence.database.service;

import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EventEntityManagement {

    @Transactional
    List<EventEntity> getAllEventsToRecreateDBFromSnapshot(Long snapshotEntity);

}
