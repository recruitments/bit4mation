package org.recruitments.bit4mation.persistence.database.repositories;

import org.recruitments.bit4mation.persistence.database.entites.NodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NodeRepository extends JpaRepository<NodeEntity, Long> {

    NodeEntity findByChildren_LeafId(Long leafId);
}
