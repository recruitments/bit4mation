package org.recruitments.bit4mation.persistence.database.entites;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "nodes")
@Getter
@Setter
@NoArgsConstructor
public class NodeEntity {

    @Id
    @GeneratedValue
    private Long nodeId;

    @ManyToOne
    @JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "node_to_parent"))
    private LeafEntity parent;

    @OneToOne
    @JoinColumn(name = "children_id", foreignKey = @ForeignKey(name = "node_to_children"), unique = true)
    private LeafEntity children;

    @Column
    private Integer weight = 0;

    public NodeEntity(LeafEntity parent, LeafEntity children, Integer weight) {
        this.parent = parent;
        this.children = children;
        this.weight = weight;
    }

}
