package org.recruitments.bit4mation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bit4MationRecruitmentTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bit4MationRecruitmentTaskApplication.class, args);
	}

}
