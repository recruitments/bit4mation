package org.recruitments.bit4mation.mappers;

import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.data.Node;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;

public class LeafEntityToTreeMapper {

    public Leaf map(LeafEntity leafEntity) {
        Leaf leaf = new Leaf(leafEntity.getLeafId(), leafEntity.getValue());
        leafEntity
                .getNodes()
                .forEach(node -> {
                    Node leafNode = new Node(node.getNodeId(), node.getWeight());
                    if (node.getChildren() != null) {
                        leafNode.setChildrenLeaf(map(node.getChildren()));
                    }
                    leaf.addNode(leafNode);
                });
        return leaf;
    }
}
