package org.recruitments.bit4mation.configuration;

import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.CreateSnapshot;
import org.recruitments.bit4mation.businesslogic.RestoreFromSnapshot;
import org.recruitments.bit4mation.businesslogic.TreeMoveManagement;
import org.recruitments.bit4mation.persistence.database.entites.LeafEntity;
import org.recruitments.bit4mation.persistence.database.service.LeafEntityManagement;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Configuration
public class DataInitializer {

    private final LeafEntityManagement leafEntityManagement;

    private final TreeMoveManagement treeMoveManagement;

    private final CreateSnapshot createSnapshot;

    private final RestoreFromSnapshot restoreFromSnapshot;

    @PostConstruct
    public void postConstruct() {
        LeafEntity leafEntity = leafEntityManagement.createRoot();
        LeafEntity firstLeaf = leafEntityManagement.createLeaf(leafEntity.getLeafId(), 3);
        createSnapshot.createSnapshot();
        LeafEntity secondLeaf = leafEntityManagement.createLeaf(leafEntity.getLeafId(), 6);
        leafEntityManagement.createLeaf(leafEntity.getLeafId(), 9);
        LeafEntity firstLeafChild = leafEntityManagement.createLeaf(firstLeaf.getLeafId(), 12);
        Long snapshot = createSnapshot.createSnapshot();
//        createSnapshot.createSnapshot();
        treeMoveManagement.moveLeaf(firstLeafChild.getLeafId(), secondLeaf.getLeafId());
//        restoreFromSnapshot.restoreFromSnapshot(snapshot);
    }

}
