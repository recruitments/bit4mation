package org.recruitments.bit4mation.data;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Leaf {

    @NonNull
    private Long leafId;

    @NonNull
    private Integer value;

    private List<Node> nodes = new ArrayList<>();

    public void addNode(Node node) {
        nodes.add(node);
    }

}
