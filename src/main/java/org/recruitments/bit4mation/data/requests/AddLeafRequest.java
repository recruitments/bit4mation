package org.recruitments.bit4mation.data.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddLeafRequest {

    private Long parentLeafId;

    private Integer weight;

}
