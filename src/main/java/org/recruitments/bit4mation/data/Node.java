package org.recruitments.bit4mation.data;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Node {

    @NonNull
    private Long nodeId;

    @NonNull
    private Integer weight;

    private Leaf childrenLeaf;

}
