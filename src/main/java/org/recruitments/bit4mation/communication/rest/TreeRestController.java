package org.recruitments.bit4mation.communication.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.recruitments.bit4mation.businesslogic.LeafCreateManagement;
import org.recruitments.bit4mation.businesslogic.TreeDeleteManagement;
import org.recruitments.bit4mation.businesslogic.TreeProvide;
import org.recruitments.bit4mation.data.Leaf;
import org.recruitments.bit4mation.data.requests.AddLeafRequest;
import org.recruitments.bit4mation.persistence.database.data.AddLeaf;
import org.recruitments.bit4mation.persistence.database.data.EventEnum;
import org.recruitments.bit4mation.persistence.database.entites.EventEntity;
import org.recruitments.bit4mation.persistence.database.entites.SnapshotEntity;
import org.recruitments.bit4mation.persistence.database.repositories.EventRepository;
import org.recruitments.bit4mation.persistence.database.repositories.SnapshotRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tree")
@RequiredArgsConstructor
public class TreeRestController {

    private final TreeProvide treeProvide;

    private final LeafCreateManagement leafCreateManagement;

    private final TreeDeleteManagement treeDeleteManagement;

    private final EventRepository eventRepository;

    private final SnapshotRepository snapshotRepository;

    @GetMapping
    public Leaf provideTree() {
        return treeProvide.provideTree();
    }

    //TODO: to debug only -> delete after
    @GetMapping("/events")
    public List<EventEntity> events() {
        eventRepository.findAll().forEach(event -> {
            if (event.getName().equals(EventEnum.CREATE_LEAF)) {
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    AddLeaf addLeaf = objectMapper.readValue(event.getPayload(), AddLeaf.class);
                    System.out.println(addLeaf);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            } else if (event.getName().equals(EventEnum.ADD_ROOT)) {
                System.out.println(Integer.valueOf(event.getPayload()));
            }
        });
        return eventRepository.findAll();
    }

    @PostMapping("/leafs")
    public Leaf addLeaf(@RequestBody AddLeafRequest addLeafRequest) {
        return leafCreateManagement.createLeaf(addLeafRequest.getParentLeafId(), addLeafRequest.getWeight());
    }

    @DeleteMapping("/leafs/{leafId}")
    public void deleteLeaf(@PathVariable Long leafId) {
        treeDeleteManagement.deleteLeaf(leafId);
    }

    @DeleteMapping("/nodes/{nodeId}")
    public void deleteNode(@PathVariable Long nodeId) {
        treeDeleteManagement.deleteNode(nodeId);
    }

    @GetMapping("/snapshots")
    public List<SnapshotEntity> snapshots() {
        return snapshotRepository.findAll();
    }

}
